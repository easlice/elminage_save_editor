#include "cli.h"

int main(int argc, char* argv[]) {
	if (argc != 2) {
		usage(argv[0]);
		exit(0);
	}

	std::ifstream file (argv[1], std::ios::binary);
	if (!file) {
		std::cerr << "Error opening the given file path: " << argv[1] << std::endl;
		exit(0);
	}

	Save game;
	game.read(file);
	file.close();
	while(1) {
		char choice;
		std::cout << "*************************" << std::endl;
		std::cout << "* L) List Character Names" << std::endl;
		std::cout << "* I) Show Detailed Character Info" << std::endl;
		std::cout << "* E) Edit Character" << std::endl;
		std::cout << "* V) Verify Characters" << std::endl;
		std::cout << "* W) Write Current Data to a Save File" << std::endl;
		std::cout << "* Q) Quit" << std::endl;
		std::cout << "*************************" << std::endl;
		std::cout << "Enter your choice and press return: ";
		std::cin >> choice;
		switch(choice) {
			case 'l':
			case 'L':
				list_char_names(game.characters);
				break;
			case 'i':
			case 'I':
				get_char_info(game.characters);
				break;
			case 'e':
			case 'E':
				edit_character(select_character(game.characters));
				break;
			case 'v':
			case 'V':
				verify_characters(game.characters);
				break;
			case 'w':
			case 'W':
				write_save_to_file(game);
				break;
			case 'q':
			case 'Q':
				exit(0);
			default:
				std::cout << ">>> That is not a valid choice" << std::endl;
		}
	}
}

void list_char_names(std::vector<Character> &chars) {
	for (Character c : chars) {
		std::cout << c.data.character_name << " \"" << c.data.character_nickname << "\" " << c.data.character_surname << std::endl;
	}
}

void write_save_to_file(Save game) {
	std::cout << "Enter the file destination and press return, or q to cancel: ";
	std::string path;
	std::cin >> path;
	if (path == "q" || path == "Q") {
		std::cout << "Canceling request to write the game state." << std::endl;
		return;
	}

	std::ofstream file (path, std::ios::binary);
	if (!file) {
		std::cerr << "Error opening the given file path: " << path << std::endl;
		return;
	}

	game.write(file);
	file.close();
	std::cout << "Game state written to the file: " << path << std::endl;
}

Character& select_character(std::vector<Character> &chars) {
	while (1) {
		std::cout << "*************************" << std::endl;
		int i = 1;
		for (Character c : chars) {
			std::cout << "* " << i << ") ";
			std::cout << c.data.character_name << " \"" << c.data.character_nickname << "\" " << c.data.character_surname << std::endl;
			i++;
		}
		std::cout << "*************************" << std::endl;
		std::cout << "Enter your choice and press return: ";
		std::cin >> i;

		if (i < 1 || i > chars.size()) {
			std::cout << ">>> That is not a valid choice." << std::endl;
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			continue;
		}

		return chars.at(i - 1);
	}
}

void edit_character(Character &c) {
	while(1) {
		char choice;
		std::cout << "*************************" << std::endl;
		std::cout << "* Editing Character: " << c.data.character_name << " \"" << c.data.character_nickname << "\" " << c.data.character_surname << std::endl;
		std::cout << "* S) edit stats" << std::endl;
		std::cout << "* A) edit Age (currently " << c.age_days() << " days)" << std::endl;
		std::cout << "* G) edit Gold (currently " << c.gp() << ")" << std::endl;
		std::cout << "* H) edit HP (currently " << c.hp_current() << "/" << c.hp_max() << ")" << std::endl;
		std::cout << "* I) print detailed info" << std::endl;
		std::cout << "* R) return" << std::endl;
		std::cout << "* " << std::endl;
		std::cout << "*************************" << std::endl;
		std::cout << "Enter your choice and press return: ";
		std::cin >> choice;

		switch (choice) {
			case 's':
			case 'S':
				edit_stats(c);
				break;
			case 'a':
			case 'A':
				edit_age(c);
				break;
			case 'g':
			case 'G':
				edit_gold(c);
				break;
			case 'h':
			case 'H':
				edit_hp(c);
				break;
			case 'i':
			case 'I':
				c.print();
				break;
			case 'r':
			case 'R':
				return;
			default:
				std::cout << ">>> That is not a valid choice" << std::endl;
		}
	}
}

void edit_stats(Character &c) {
	while(1) {
		char choice;
		std::cout << "*************************" << std::endl;
		std::cout << "* Editing Character: " << c.data.character_name << " \"" << c.data.character_nickname << "\" " << c.data.character_surname << std::endl;
		std::cout << "* S) Edit Strength (currently " << (unsigned int) c.strength() << ")" << std::endl;
		std::cout << "* I) Edit Intelligence (currently " << (unsigned int) c.intelligence() << ")" << std::endl;
		std::cout << "* P) Edit Piety (currently " << (unsigned int) c.piety() << ")" << std::endl;
		std::cout << "* V) Edit Vitality (currently " << (unsigned int) c.vitality() << ")" << std::endl;
		std::cout << "* A) Edit Agility (currently " << (unsigned int) c.agility() << ")" << std::endl;
		std::cout << "* L) Edit Luck (currently " << (unsigned int) c.luck() << ")" << std::endl;
		std::cout << "* R) Return" << std::endl;
		std::cout << "*************************" << std::endl;
		std::cout << "Enter your choice and press return: ";
		std::cin >> choice;

		switch(choice) {
			case 's':
			case 'S':
				set_stat("Strength", c.strength(), c, &Character::strength);
				break;
			case 'i':
			case 'I':
				set_stat("Intelligence", c.intelligence(), c, &Character::intelligence);
				break;
			case 'p':
			case 'P':
				set_stat("Piety", c.piety(), c, &Character::piety);
				break;
			case 'v':
			case 'V':
				set_stat("Vitality", c.vitality(), c, &Character::vitality);
				break;
			case 'a':
			case 'A':
				set_stat("Agility", c.agility(), c, &Character::agility);
				break;
			case 'l':
			case 'L':
				set_stat("Luck", c.luck(), c, &Character::luck);
				break;
			case 'r':
			case 'R':
				return;
			default:
				std::cout << ">>> That is not a valid choice" << std::endl;
		}
	}
}

// I am honestly unsure if the way this method works is really clever...
// ... or a terrible abomination. On the one hand, if I have the opportunity
// to wonder about it, it's probably the latter. On the other hand, it means
// not creating 6 different methods, one for each stat, that all do the same
// thing and just call a slightly different function.
void set_stat(std::string stat_name, const unsigned char current_val, Character& c, unsigned char (Character::*fpntr)(const unsigned char)) {
	while (1) {
		std::uint32_t new_val;
		std::cout << "Current " << stat_name << ": " << (unsigned int) current_val << std::endl;
		std::cout << "Enter your new age " << stat_name << " and press return: ";
		std::cin >> new_val;

		std::cout << "Old " << stat_name << " '" << (unsigned int) current_val << "' and new " << stat_name << " '" << new_val << "'" << std::endl;
		if (confirm()) {
			(c.*fpntr)(new_val);
			return;
		} else {
			return;
		}
	}
}

void edit_hp(Character &c) {
	while(1) {
		char choice;
		std::cout << "*************************" << std::endl;
		std::cout << "* Editing Character: " << c.data.character_name << " \"" << c.data.character_nickname << "\" " << c.data.character_surname << std::endl;
		std::cout << "* C) Edit Current HP (currently " << (unsigned int) c.hp_current() << ")" << std::endl;
		std::cout << "* M) Edit Max HP (currently " << (unsigned int) c.hp_max() << ")" << std::endl;
		std::cout << "* R) Return" << std::endl;
		std::cout << "*************************" << std::endl;
		std::cout << "Enter your choice and press return: ";
		std::cin >> choice;

		switch(choice) {
			case 'c':
			case 'C':
				set_hp_current(c);
				break;
			case 'm':
			case 'M':
				set_hp_max(c);
				break;
			case 'r':
			case 'R':
				return;
			default:
				std::cout << ">>> That is not a valid choice" << std::endl;
		}
	}
}

void set_hp_current(Character &c) {
	while (1) {
		std::uint32_t new_val;
		std::cout << "Current HP: " << (unsigned int) c.hp_current() << std::endl;
		std::cout << "Enter your new current HP and press return: ";
		std::cin >> new_val;

		std::cout << "Old current HP '" << (unsigned int) c.hp_current() << "' and new current HP '" << new_val << "'" << std::endl;
		if (confirm()) {
			c.hp_current(new_val);
			return;
		} else {
			return;
		}
	}
}

void set_hp_max(Character &c) {
	while (1) {
		std::uint32_t new_val;
		std::cout << "Max HP: " << (unsigned int) c.hp_max() << std::endl;
		std::cout << "Enter your new max HP and press return: ";
		std::cin >> new_val;

		std::cout << "Old max HP '" << (unsigned int) c.hp_max() << "' and new max HP '" << new_val << "'" << std::endl;
		if (confirm()) {
			c.hp_max(new_val);
			return;
		} else {
			return;
		}
	}
}

void edit_age (Character &c) {
	while (1) {
		std::uint32_t age = c.age_days();
		std::uint32_t new_age;
		std::cout << "Current age in days: " << age << std::endl;
		std::cout << "Enter your new age value in days and press return: ";
		std::cin >> new_age;

		std::cout << "Old age in days '" << age << "' and new age in days '" << new_age << "'" << std::endl;
		if (confirm()) {
			c.age_days(new_age);
			return;
		} else {
			return;
		}
	}
}

void edit_gold (Character &c) {
	while (1) {
		std::uint64_t gp = c.gp();
		std::uint64_t new_gp;
		std::cout << "Current gp: " << gp << std::endl;
		std::cout << "Enter your new gp value and press return: ";
		std::cin >> new_gp;

		std::cout << "Old gp '" << gp << "' and new gp '" << new_gp << "'" << std::endl;
		if (confirm()) {
			c.gp(new_gp);
			return;
		} else {
			return;
		}
	}
}

bool confirm() {
	while (1) {
		char choice;
		std::cout << "Are you sure? [y/n] ";
		std::cin >> choice;

		switch (choice) {
			case 'y':
			case 'Y':
				return true;
			case 'n':
			case 'N':
				return false;
		}
	}
}

void verify_characters(std::vector<Character> &chars) {
	for (Character c : chars) {
		c.verify_mappings();
	}
}

void get_char_info(std::vector<Character> &chars) {
	select_character(chars).print();
}


void usage(char* name) {
	std::cout << "Usage: " << name << " [FILE]" << std::endl;
}
