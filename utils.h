#ifndef UTILS_H
#define UTIL_H

#include <cstdint>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <array>
#include <stdexcept>
#include "value_labels.h"

std::array<unsigned char, 4> get_uchar_array_from_uint32(const std::uint32_t in_val);
std::uint32_t get_uint32_from_uchar_array(const std::array<unsigned char, 4> arr);
std::array<unsigned char, 8> get_uchar_array_from_uint64(const std::uint64_t in_val);
std::uint64_t get_uint64_from_uchar_array(const std::array<unsigned char, 8> arr);
#define PRINT_HEX(a) {\
	std::cout << std::setw(2) << std::setfill('0') << std::hex << (unsigned int) a;\
}

#define PRINT_MAP_VALUE(a,b) {\
	std::cout << " => " << b.left(a);\
}

#define PRINT_NAME(a) {\
	std::cout << a << " : ";\
}

#define PRINTLN(a) {a; std::cout << std::endl;}

void print_array_as_hex (void *arr, unsigned int size);

#define PRINT_ARRAY_AS_HEX(a) {\
	print_array_as_hex(a.data(), a.size());\
}

unsigned long long get_little_endian_long(unsigned char* arr, unsigned int size);

#define GET_LITTLE_ENDIAN_LONG(a) get_little_endian_long(a.data(), a.size())

#define PRINT_ARRAY_HEX_AND_VALUE(a) {\
	PRINT_ARRAY_AS_HEX(a);\
	std::cout << " <=> " << std::dec << GET_LITTLE_ENDIAN_LONG(a);\
}

void print_traits(unsigned char* traits, unsigned int size);

#define PRINT_TRAITS(a) print_traits(a.data(), a.size())

bool doesFilePointAtCharacter(std::ifstream& file);

#endif // UTILS_H
