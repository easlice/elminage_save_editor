#include "character.h"

void Character::print() {
	// TODO: Remove or fix this terrible, terrible mess
	// And if anyone reads this, I'm sorry, I know it is ugly and full of Macros.
	// Thankfully, it only really exists for debug proposes.
	std::cout << std::setw(2) << std::setfill('0');
	std::cout << "Name: " << this->data.character_name << std::endl;
	std::cout << "Surname: " << this->data.character_surname << std::endl;
	std::cout << "Nickname: " << this->data.character_nickname << std::endl;
	PRINTLN(PRINT_NAME("Unkown 1");PRINT_ARRAY_HEX_AND_VALUE(this->data.unknown1));
	std::cout << "Alignment Nickname: " << this->data.alignment_nickname << std::endl;
	PRINTLN(PRINT_NAME("Icon");PRINT_ARRAY_HEX_AND_VALUE(this->data.icon));
	PRINTLN(PRINT_NAME("Level");PRINT_ARRAY_HEX_AND_VALUE(this->data.level));
	PRINTLN(PRINT_NAME("Age (days)");PRINT_ARRAY_HEX_AND_VALUE(this->data.age_days));
	PRINTLN(PRINT_NAME("Race");PRINT_HEX(this->race());PRINT_MAP_VALUE(this->race(),RAW_VALUE_MAPS::RACE));
	PRINTLN(PRINT_NAME("Gender");PRINT_HEX(this->gender());PRINT_MAP_VALUE(this->gender(),RAW_VALUE_MAPS::GENDER));
	PRINTLN(PRINT_NAME("Unkown 2");PRINT_ARRAY_HEX_AND_VALUE(this->data.unknown2));
	PRINTLN(PRINT_NAME("HP (current)");PRINT_ARRAY_HEX_AND_VALUE(this->data.hp_current));
	PRINTLN(PRINT_NAME("HP (max)");PRINT_ARRAY_HEX_AND_VALUE(this->data.hp_max));
	PRINTLN(PRINT_NAME("Unkown 3");PRINT_ARRAY_HEX_AND_VALUE(this->data.unknown3));
	PRINTLN(PRINT_NAME("Stats");PRINT_ARRAY_HEX_AND_VALUE(this->data.stats));
	std::cout << "Strength: "     << std::dec << (unsigned int) this->strength()     << std::endl;
	std::cout << "Intelligence: " << std::dec << (unsigned int) this->intelligence() << std::endl;
	std::cout << "Piety: "        << std::dec << (unsigned int) this->piety()        << std::endl;
	std::cout << "Vitality: "     << std::dec << (unsigned int) this->vitality()     << std::endl;
	std::cout << "Agility: "      << std::dec << (unsigned int) this->agility()      << std::endl;
	std::cout << "Luck: "         << std::dec << (unsigned int) this->luck()         << std::endl;
	PRINTLN(PRINT_NAME("Unkown 4");PRINT_ARRAY_HEX_AND_VALUE(this->data.unknown4));
	PRINTLN(PRINT_NAME("Kills");PRINT_ARRAY_HEX_AND_VALUE(this->data.kills));
	PRINTLN(PRINT_NAME("Exp");PRINT_ARRAY_HEX_AND_VALUE(this->data.exp));
	PRINTLN(PRINT_NAME("GP");PRINT_ARRAY_HEX_AND_VALUE(this->data.gp));
	PRINTLN(PRINT_NAME("Unkown 5");PRINT_ARRAY_HEX_AND_VALUE(this->data.unknown5));
	PRINTLN(PRINT_NAME("Mage Spells (current)");PRINT_ARRAY_AS_HEX(this->data.mage_current_mp));
	PRINTLN(PRINT_NAME("Priest Spells (current)");PRINT_ARRAY_AS_HEX(this->data.priest_current_mp));
	PRINTLN(PRINT_NAME("Alchemist Spells (current)");PRINT_ARRAY_AS_HEX(this->data.alchemist_current_mp));
	PRINTLN(PRINT_NAME("Summoner Spells (current)");PRINT_ARRAY_AS_HEX(this->data.summoner_current_mp));
	PRINTLN(PRINT_NAME("Mage Spells (max)");PRINT_ARRAY_AS_HEX(this->data.mage_max_mp));
	PRINTLN(PRINT_NAME("Priest Spells (max)");PRINT_ARRAY_AS_HEX(this->data.priest_max_mp));
	PRINTLN(PRINT_NAME("Alchemist Spells (max)");PRINT_ARRAY_AS_HEX(this->data.alchemist_max_mp));
	PRINTLN(PRINT_NAME("Summoner Spells (max)");PRINT_ARRAY_AS_HEX(this->data.summoner_max_mp));
	PRINTLN(PRINT_NAME("Unknown 6");PRINT_ARRAY_HEX_AND_VALUE(this->data.unknown6));
	PRINT_TRAITS(this->data.traits);
	PRINTLN(PRINT_NAME("EX Skill");PRINT_ARRAY_AS_HEX(this->data.ex_skill);PRINT_MAP_VALUE(this->data.ex_skill[0], RAW_VALUE_MAPS::SKILLS));

	std::cout << "\nREST: ";
	print_array_as_hex(this->data.rest, this->data.rest_size);
	std::cout << std::endl;
}

unsigned char Character::gender() const {
	return (this->data.race_gender[0] & 0b11110000) >> 4;
}
unsigned char Character::gender(const unsigned char in) {
	this->data.race_gender[0] = (this->data.race_gender[0] & 0b00001111) | (in << 4);
	return this->gender();
}

unsigned char Character::race(const unsigned char in) {
	this->data.race_gender[0] = (in & 0b00001111) | (this->data.race_gender[0] & 0b11110000);
	return this->race();
}
unsigned char Character::race() const {
	return (this->data.race_gender[0] & 0b00001111);
}

unsigned char Character::strength(const unsigned char in) {
	if ((in & 0b11000000) != 0) { throw std::invalid_argument("Given stat value is too high. max is 63"); }
	this->data.stats[0] = (this->data.stats[0] & 0b11000000) | (in & 0b00111111);
	return this->strength();
}
unsigned char Character::strength() const {
	return (this->data.stats[0] & 0b00111111);
}

unsigned char Character::intelligence(const unsigned char in) {
	if ((in & 0b11000000) != 0) { throw std::invalid_argument("Given stat value is too high. max is 63"); }
	this->data.stats[0] = ((in & 0b00000011) << 6) | (this->data.stats[0] & 0b00111111);
	this->data.stats[1] = (this->data.stats[1] & 0b11110000) | ((in & 0b00111100) >> 2);
	return this->intelligence();
}
unsigned char Character::intelligence() const {
	return ((this->data.stats[0] & 0b11000000) >> 6) | ((this->data.stats[1] & 0b00001111) << 2);
}

unsigned char Character::piety(const unsigned char in) {
	if ((in & 0b11000000) != 0) { throw std::invalid_argument("Given stat value is too high. max is 63"); }
	this->data.stats[1] = ((in & 0b00001111) << 4) | (this->data.stats[1] & 0b00001111);
	this->data.stats[2] = (this->data.stats[2] & 0b11111100) | ((in & 0b00110000) >> 4);
	return this->piety();
}
unsigned char Character::piety() const {
	return ((this->data.stats[1] & 0b11110000) >> 4) | ((this->data.stats[2] & 0b00000011) << 4);
}

unsigned char Character::vitality(const unsigned char in) {
	if ((in & 0b11000000) != 0) { throw std::invalid_argument("Given stat value is too high. max is 63"); }
	this->data.stats[2] = ((in & 0b00111111) << 2) | (this->data.stats[2] & 0b00000011);
	return this->vitality();
}
unsigned char Character::vitality() const {
	return ((this->data.stats[2] & 0b11111100) >> 2);
}

unsigned char Character::agility(const unsigned char in) {
	if ((in & 0b11000000) != 0) { throw std::invalid_argument("Given stat value is too high. max is 63"); }
	this->data.stats[3] = (this->data.stats[3] & 0b11000000) | (in & 0b00111111);
	return this->agility();
}
unsigned char Character::agility() const {
	return (this->data.stats[3] & 0b00111111);
}

unsigned char Character::luck(const unsigned char in) {
	if ((in & 0b11000000) != 0) { throw std::invalid_argument("Given stat value is too high. max is 63"); }
	this->data.stats[4] = (this->data.stats[4] & 0b11000000) | (in & 0b00111111);
	return this->luck();
}
unsigned char Character::luck() const {
	return (this->data.stats[4] & 0b00111111);
}

std::uint32_t Character::hp_current(const std::uint32_t in_val) {
	this->data.hp_current = get_uchar_array_from_uint32(in_val);
	return this->hp_current();
}
std::uint32_t Character::hp_current() const {
	return get_uint32_from_uchar_array(this->data.hp_current);
}

std::uint32_t Character::hp_max(const std::uint32_t in_val) {
	this->data.hp_max = get_uchar_array_from_uint32(in_val);
	return this->hp_max();
}
std::uint32_t Character::hp_max() const {
	return get_uint32_from_uchar_array(this->data.hp_max);
}

std::uint32_t Character::exp(const std::uint32_t in_val) {
	this->data.exp = get_uchar_array_from_uint32(in_val);
	return this->exp();
}
std::uint32_t Character::exp() const {
	return get_uint32_from_uchar_array(this->data.exp);
}

std::uint64_t Character::gp(const std::uint64_t in_val) {
	std::array<unsigned char, 8> converted_gp = get_uchar_array_from_uint64(in_val);
	std::array<unsigned char, 6> real_gp;
	// start from the left hand side since these are little endian
	for (int i = 0; i < 6; ++i) {
		real_gp[i] = converted_gp[i];
	}
	this->data.gp = real_gp;
	return this->gp();
}
std::uint64_t Character::gp() const {
	std::array<unsigned char, 6> real_gp = this->data.gp;
	std::array<unsigned char, 8> converted_gp;
	// start from the left hand side
	for (int i = 0; i < 6; ++i) {
		converted_gp[i] = real_gp[i];
	}
	return get_uint64_from_uchar_array(converted_gp);
}

std::uint32_t Character::age_days(const std::uint32_t in_val) {
	this->data.age_days = get_uchar_array_from_uint32(in_val);
	return this->age_days();
}
std::uint32_t Character::age_days() const {
	return get_uint32_from_uchar_array(this->data.age_days);
}

std::array<std::uint8_t,9> Character::mage_current_mp(const std::array<std::uint8_t,9> in_val) {
	for (int i = 0; i < 9; ++i) {
		this->data.mage_current_mp[i] = (unsigned char) in_val[i];
	}
	return this->mage_current_mp();
}
std::array<std::uint8_t,9> Character::mage_current_mp() const {
	std::array<std::uint8_t,9> mp;
	for(int i = 0; i < 9; ++i) {
		mp[i] = this->data.mage_current_mp[i];
	}
	return mp;
}
std::array<std::uint8_t,9> Character::priest_current_mp(const std::array<std::uint8_t,9> in_val) {
	for (int i = 0; i < 9; ++i) {
		this->data.priest_current_mp[i] = (unsigned char) in_val[i];
	}
	return this->priest_current_mp();
}
std::array<std::uint8_t,9> Character::priest_current_mp() const {
	std::array<std::uint8_t,9> mp;
	for(int i = 0; i < 9; ++i) {
		mp[i] = this->data.priest_current_mp[i];
	}
	return mp;
}
std::array<std::uint8_t,9> Character::alchemist_current_mp(const std::array<std::uint8_t,9> in_val) {
	for (int i = 0; i < 9; ++i) {
		this->data.alchemist_current_mp[i] = (unsigned char) in_val[i];
	}
	return this->alchemist_current_mp();
}
std::array<std::uint8_t,9> Character::alchemist_current_mp() const {
	std::array<std::uint8_t,9> mp;
	for(int i = 0; i < 9; ++i) {
		mp[i] = this->data.alchemist_current_mp[i];
	}
	return mp;
}
std::array<std::uint8_t,9> Character::summoner_current_mp(const std::array<std::uint8_t,9> in_val) {
	for (int i = 0; i < 9; ++i) {
		this->data.summoner_current_mp[i] = (unsigned char) in_val[i];
	}
	return this->summoner_current_mp();
}
std::array<std::uint8_t,9> Character::summoner_current_mp() const {
	std::array<std::uint8_t,9> mp;
	for(int i = 0; i < 9; ++i) {
		mp[i] = this->data.summoner_current_mp[i];
	}
	return mp;
}

std::array<std::uint8_t,9> Character::mage_max_mp(const std::array<std::uint8_t,9> in_val) {
	for (int i = 0; i < 9; ++i) {
		this->data.mage_max_mp[i] = (unsigned char) in_val[i];
	}
	return this->mage_max_mp();
}
std::array<std::uint8_t,9> Character::mage_max_mp() const {
	std::array<std::uint8_t,9> mp;
	for(int i = 0; i < 9; ++i) {
		mp[i] = this->data.mage_max_mp[i];
	}
	return mp;
}
std::array<std::uint8_t,9> Character::priest_max_mp(const std::array<std::uint8_t,9> in_val) {
	for (int i = 0; i < 9; ++i) {
		this->data.priest_max_mp[i] = (unsigned char) in_val[i];
	}
	return this->priest_max_mp();
}
std::array<std::uint8_t,9> Character::priest_max_mp() const {
	std::array<std::uint8_t,9> mp;
	for(int i = 0; i < 9; ++i) {
		mp[i] = this->data.priest_max_mp[i];
	}
	return mp;
}
std::array<std::uint8_t,9> Character::alchemist_max_mp(const std::array<std::uint8_t,9> in_val) {
	for (int i = 0; i < 9; ++i) {
		this->data.alchemist_max_mp[i] = (unsigned char) in_val[i];
	}
	return this->alchemist_max_mp();
}
std::array<std::uint8_t,9> Character::alchemist_max_mp() const {
	std::array<std::uint8_t,9> mp;
	for(int i = 0; i < 9; ++i) {
		mp[i] = this->data.alchemist_max_mp[i];
	}
	return mp;
}
std::array<std::uint8_t,9> Character::summoner_max_mp(const std::array<std::uint8_t,9> in_val) {
	for (int i = 0; i < 9; ++i) {
		this->data.summoner_max_mp[i] = (unsigned char) in_val[i];
	}
	return this->summoner_max_mp();
}
std::array<std::uint8_t,9> Character::summoner_max_mp() const {
	std::array<std::uint8_t,9> mp;
	for(int i = 0; i < 9; ++i) {
		mp[i] = this->data.summoner_max_mp[i];
	}
	return mp;
}

std::array<unsigned char, 7> Character::traits(std::array<unsigned char, 7> in_val) {
	this->data.traits = in_val;
	return this->traits();
}
std::array<unsigned char, 7> Character::traits() {
	return this->data.traits;
}

unsigned char Character::ex_skill(unsigned char in_val) {
	this->data.ex_skill[0] = in_val;
	return this->ex_skill();
}
unsigned char Character::ex_skill() {
	return this->data.ex_skill[0];
}

void Character::read (std::ifstream& file) {
	#define __READ_ARRAY(a) {\
		file.read((char*) a.data(), a.size());\
	}

	const unsigned int start_pos = file.tellg();
	Character c;
	file.read(this->data.character_name, 27);
	file.read(this->data.character_surname, 23);
	file.read(this->data.character_nickname, 19);
	__READ_ARRAY(this->data.unknown1); // Get data in first unknown block
	file.read(this->data.alignment_nickname, 13);
	__READ_ARRAY(this->data.icon);
	__READ_ARRAY(this->data.level);
	__READ_ARRAY(this->data.age_days);
	__READ_ARRAY(this->data.race_gender);
	__READ_ARRAY(this->data.unknown2); // Get the data for class, alignment, and AC?
	__READ_ARRAY(this->data.hp_current);
	__READ_ARRAY(this->data.hp_max);
	__READ_ARRAY(this->data.unknown3); // control and unknowns
	__READ_ARRAY(this->data.stats);
	__READ_ARRAY(this->data.unknown4); // Unknown...
	__READ_ARRAY(this->data.kills);
	__READ_ARRAY(this->data.exp);
	__READ_ARRAY(this->data.gp);
	__READ_ARRAY(this->data.unknown5); // Inventory?
	__READ_ARRAY(this->data.mage_current_mp);
	__READ_ARRAY(this->data.priest_current_mp);
	__READ_ARRAY(this->data.alchemist_current_mp);
	__READ_ARRAY(this->data.summoner_current_mp);
	__READ_ARRAY(this->data.mage_max_mp);
	__READ_ARRAY(this->data.priest_max_mp);
	__READ_ARRAY(this->data.alchemist_max_mp);
	__READ_ARRAY(this->data.summoner_max_mp);
	__READ_ARRAY(this->data.unknown6);
	__READ_ARRAY(this->data.traits);
	__READ_ARRAY(this->data.ex_skill);

	const unsigned int last_pos = file.tellg();
	this->data.rest_size = this->data.SIZE - (last_pos - start_pos);
	this->data.rest = new char[this->data.rest_size];
	file.read(this->data.rest, this->data.rest_size);
	#undef __READ_ARRAY
}

void Character::write(std::ofstream& file) {
	#define __WRITE_ARRAY(a) {\
		file.write((char*) a.data(), a.size());\
	}

	file.write(this->data.character_name, 27);
	file.write(this->data.character_surname, 23);
	file.write(this->data.character_nickname, 19);
	__WRITE_ARRAY(this->data.unknown1); // Get data in first unknown block
	file.write(this->data.alignment_nickname, 13);
	__WRITE_ARRAY(this->data.icon);
	__WRITE_ARRAY(this->data.level);
	__WRITE_ARRAY(this->data.age_days);
	__WRITE_ARRAY(this->data.race_gender);
	__WRITE_ARRAY(this->data.unknown2); // Get the data for class, alignment, and AC?
	__WRITE_ARRAY(this->data.hp_current);
	__WRITE_ARRAY(this->data.hp_max);
	__WRITE_ARRAY(this->data.unknown3); // control and unknowns
	__WRITE_ARRAY(this->data.stats);
	__WRITE_ARRAY(this->data.unknown4); // Unknown...
	__WRITE_ARRAY(this->data.kills);
	__WRITE_ARRAY(this->data.exp);
	__WRITE_ARRAY(this->data.gp);
	__WRITE_ARRAY(this->data.unknown5); // Inventory?
	__WRITE_ARRAY(this->data.mage_current_mp);
	__WRITE_ARRAY(this->data.priest_current_mp);
	__WRITE_ARRAY(this->data.alchemist_current_mp);
	__WRITE_ARRAY(this->data.summoner_current_mp);
	__WRITE_ARRAY(this->data.mage_max_mp);
	__WRITE_ARRAY(this->data.priest_max_mp);
	__WRITE_ARRAY(this->data.alchemist_max_mp);
	__WRITE_ARRAY(this->data.summoner_max_mp);
	__WRITE_ARRAY(this->data.unknown6);
	__WRITE_ARRAY(this->data.traits);
	__WRITE_ARRAY(this->data.ex_skill);
	file.write(this->data.rest, this->data.rest_size);

	#undef __WRITE_ARRAY
}

bool Character::verify_mappings() {
	#define CHECK_MAP_VALUE(a,b) {\
		try {\
			b.left_map().at(a);\
		} catch (std::out_of_range e) {\
			std::cerr << "No mapping found for value '" << (unsigned int) a << "' in map '" << #b << "'" << std::endl;\
			this->print();\
		}\
	}
	CHECK_MAP_VALUE(this->race(), RAW_VALUE_MAPS::RACE);
	CHECK_MAP_VALUE(this->gender(), RAW_VALUE_MAPS::GENDER);
	for (unsigned char t : this->traits()) {
		CHECK_MAP_VALUE(t, RAW_VALUE_MAPS::TRAITS);
	}
	CHECK_MAP_VALUE(this->ex_skill(), RAW_VALUE_MAPS::SKILLS);
	#undef CHECK_MAP_VALUE
}
