#pragma once
#ifndef CHARACTER_H
#define CHARACTER_H

/**
 * A character is a block of bytes in the save file. The following are the sections:
 * Offset  | Field                          | Length   | Notes
 *         |                                |          |
 * 0       | Character Name                 | 27 bytes |
 * 1B      | Character Surname              | 23 bytes |
 * 32      | Character Nickname             | 19 bytes |
 * 45      | ???                            | 6 bytes  | ???
 * 4B      | Alignment Nickname             | 13 bytes |
 * 58      | Icon                           | 2 bytes  |
 * 5A      | Level                          | 2 bytes  |
 * 5C      | Age in days                    | 4 bytes  | little endian
 * 60      | Race and Gender                | 1 byte   | left nibble gender, right nibble race
 * 61      | Alignment and Class            | 3 bytes  | Unsure if nibbles or list. Might just be 1 byte.
 * 64      | Base AC?                       | 4 bytes? | Not sure about this
 * 68      | Current HP                     | 4 bytes? |
 * 6C      | Max HP                         | 4 bytes? |
 * 70      | (Non) Player Control?          | ???      | 00 => Computer Controlled, 3F => Player Controlled
 * 78      | Stats                          | 5 bytes  | 6 bits per stat
 *         |                                |          | This looks like this, starting at 78, read right from left per byte
 *         |                                |          | (s = str, i = int, p = pie, v = vit, a = agi, l = luck)
 *         |                                |          | iissssss ppppiiii vvvvvvpp ??aaaaaa ??llllll
 * 7D      | ?                              | 7 bytes? |
 * 84      | Kills                          | 4 bytes? | little endian?
 * 88      | Exp                            | 4 bytes? | little endian
 * 8C      | GP                             | 6 bytes? | little endian?
 * 92      | ?                              |          | 07 for bishop
 * 93      | ?                              |          |
 * 94      | ?                              |          | 80 for bishop
 * 95      | ?                              |          | 03 for bishop
 * 96      | ?                              |          |
 * 97      | ?                              |          |
 * 98      | ?                              |          | FC for Alchemist
 * 99      | ?                              |          | 01 for Alchemist
 * 9A      | ?                              |          |
 * 9B      | ?                              |          | 0E for Summoner
 * ???
 * A2      | Mage Current MP                | 7 bytes  |
 * A9      | Priest Current MP              | 7 bytes  |
 * B0      | Alchemist Current MP           | 7 bytes  |
 * B7      | Summoner Current MP            | 7 bytes  |
 * BF      | Mage Max MP                    | 7 bytes  |
 * C5      | Priest Max MP                  | 7 bytes  |
 * CC      | Alchemist Max MP               | 7 bytes  |
 * D3      | Summoner Max MP                | 7 bytes  |
 * DA      | ?                              |          |
 * DB      | ?                              |          |
 * DC      | ?                              |          |
 * DD      | ?                              |          |
 * DE      | Traits                         | 7 bytes  | see value map for possible values
 * E5      | Extra Skill                    | 1 byte   | see value map for possible values
 * E6      | Status Recovery Rate?          | 2 bytes? |
 * E8      | Regen?                         |          |
 * ???
 * EC - F5 | extra damage by creature type? |          | Look more at this later
 * ???
 * 3DC     | last byte                      | ----     | next byte is next char
 */
#include <iostream>
#include <fstream>
#include <iomanip>
#include <array>
#include <stdexcept>
#include "value_labels.h"
#include "utils.h"

class Character {
	public:
		struct character_raw_data {
			static const unsigned int SIZE = 988;

			char character_name[27] = "";
			char character_surname[23] = "";
			char character_nickname[19] = "";

			std::array<unsigned char, 6> unknown1;

			char alignment_nickname[13] = "";
			std::array<unsigned char, 2> icon;
			std::array<unsigned char, 2> level;
			std::array<unsigned char, 4> age_days;
			std::array<unsigned char, 1> race_gender;

			// TODO: add alignment and class
			// TODO: add AC
			std::array<unsigned char, 7> unknown2;

			std::array<unsigned char, 4> hp_current;
			std::array<unsigned char, 4> hp_max;

			// TODO: add control?
			std::array<unsigned char, 8> unknown3;

			std::array<unsigned char, 5> stats;

			// TODO: No idea what this gap is
			std::array<unsigned char, 7> unknown4;

			std::array<unsigned char, 4> kills;
			std::array<unsigned char, 4> exp;
			std::array<unsigned char, 6> gp; // maybe only 4?

			// TODO: Control and unknown?
			std::array<unsigned char, 16> unknown5;

			std::array<unsigned char, 7> mage_current_mp;
			std::array<unsigned char, 7> priest_current_mp;
			std::array<unsigned char, 7> alchemist_current_mp;
			std::array<unsigned char, 7> summoner_current_mp;
			std::array<unsigned char, 7> mage_max_mp;
			std::array<unsigned char, 7> priest_max_mp;
			std::array<unsigned char, 7> alchemist_max_mp;
			std::array<unsigned char, 7> summoner_max_mp;

			// TODO: No idea
			std::array<unsigned char, 4> unknown6;

			std::array<unsigned char, 7> traits;
			std::array<unsigned char, 1> ex_skill;

			unsigned int rest_size;
			char* rest = NULL;
		} data;

		void print();

		unsigned char race() const;
		unsigned char race(const unsigned char in);
		unsigned char gender() const;
		unsigned char gender(const unsigned char in);

		unsigned char strength() const;
		unsigned char strength(const unsigned char in);
		unsigned char intelligence() const;
		unsigned char intelligence(const unsigned char in);
		unsigned char piety() const;
		unsigned char piety(const unsigned char in);
		unsigned char vitality() const;
		unsigned char vitality(const unsigned char in);
		unsigned char agility() const;
		unsigned char agility(const unsigned char in);
		unsigned char luck() const;
		unsigned char luck(const unsigned char in);

		std::uint32_t hp_current(const std::uint32_t in_val);
		std::uint32_t hp_current() const;
		std::uint32_t hp_max(const std::uint32_t in_val);
		std::uint32_t hp_max() const;

		std::uint32_t exp(const std::uint32_t in_val);
		std::uint32_t exp() const;
		std::uint64_t gp(const std::uint64_t in_val);
		std::uint64_t gp() const;
		std::uint32_t age_days(const std::uint32_t in_val);
		std::uint32_t age_days() const;

		std::array<std::uint8_t,9> mage_current_mp(const std::array<std::uint8_t,9> in_val);
		std::array<std::uint8_t,9> mage_current_mp() const;
		std::array<std::uint8_t,9> priest_current_mp(const std::array<std::uint8_t,9> in_val);
		std::array<std::uint8_t,9> priest_current_mp() const;
		std::array<std::uint8_t,9> alchemist_current_mp(const std::array<std::uint8_t,9> in_val);
		std::array<std::uint8_t,9> alchemist_current_mp() const;
		std::array<std::uint8_t,9> summoner_current_mp(const std::array<std::uint8_t,9> in_val);
		std::array<std::uint8_t,9> summoner_current_mp() const;

		std::array<std::uint8_t,9> mage_max_mp(const std::array<std::uint8_t,9> in_val);
		std::array<std::uint8_t,9> mage_max_mp() const;
		std::array<std::uint8_t,9> priest_max_mp(const std::array<std::uint8_t,9> in_val);
		std::array<std::uint8_t,9> priest_max_mp() const;
		std::array<std::uint8_t,9> alchemist_max_mp(const std::array<std::uint8_t,9> in_val);
		std::array<std::uint8_t,9> alchemist_max_mp() const;
		std::array<std::uint8_t,9> summoner_max_mp(const std::array<std::uint8_t,9> in_val);
		std::array<std::uint8_t,9> summoner_max_mp() const;

		std::array<unsigned char, 7> traits(std::array<unsigned char, 7> in_val);
		std::array<unsigned char, 7> traits();
		unsigned char ex_skill(unsigned char in_val);
		unsigned char ex_skill();

		void read(std::ifstream& file);
		void write(std::ofstream& file);
		bool verify_mappings();
};

#endif // CHARACTER_H
