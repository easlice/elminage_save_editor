#include "header.h"

void Header::read(std::ifstream& file) {
	file.read((char*) this->header.data(), this->header.size());
}

void Header::write(std::ofstream& file) {
	file.write((char*) this->header.data(), this->header.size());
}
