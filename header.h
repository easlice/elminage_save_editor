#pragma once
#ifndef HEADER_H
#define HEADER_H

#include <array>
#include <iostream>
#include <fstream>

class Header {
	public:
		static const unsigned int SIZE = 72;

		std::array<unsigned char, 72> header;

		void write (std::ofstream& file);
		void read (std::ifstream& file);
};
#endif // HEADER_H
