CXX=g++
CXX_FLAGS=-std=c++11 -g

OBJ_DIR = obj
BIN_DIR = bin
_OBJS = character.o cli.o value_labels.o save.o header.o utils.o
CLI_NAME = elminage_save_editor_cli
OBJS = $(patsubst %,$(OBJ_DIR)/%,$(_OBJS))
RM=rm -f

cli: $(OBJS)
	$(CXX) $(CXX_FLAGS) $(OBJS) -o $(BIN_DIR)/$(CLI_NAME)

$(OBJ_DIR)/%.o: %.cpp %.h
	$(CXX) $(CXX_FLAGS) -o $@ -c $<

clean:
	$(RM) $(OBJ_DIR)/* $(BIN_DIR)/*

character.h: value_labels.h utils.h

cli.h: character.h save.h header.h utils.h

save.h: character.h header.h utils.h

utils.h: value_labels.h
