#pragma once
#ifndef BIMAP_H
#define BIMAP_H

#include <map>

template <class T, class K>
class bimap {
	public:
		K left(T key);
		T right(K key);
		void add(T lhs, K rhs);
		std::map<T, K>& left_map();
		std::map<K, T>& right_map();

		bimap(std::initializer_list<std::pair<T,K>> values) {
			for(std::pair<T,K> p : values) {
				this->add(p.first, p.second);
			}
		};

		bimap() {};

	private:
		std::map<T, K> _left;
		std::map<K, T> _right;
};

template<class T, class K>
void bimap<T, K>::add(T lhs, K rhs) {
	_left[lhs] = rhs;
	_right[rhs] = lhs;
}

template<class T, class K>
K bimap<T, K>::left(T key) {
	return this->_left[key];
}

template<class T, class K>
T bimap<T, K>::right(K key) {
	return this->_right[key];
}

template<class T, class K>
std::map<T, K>& bimap<T, K>::left_map() {
	return this->_left;
}

template<class T, class K>
std::map<K, T>& bimap<T, K>::right_map() {
	return this->_right;
}

#endif // BIMAP_H
