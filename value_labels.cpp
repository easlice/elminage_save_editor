#include "value_labels.h"

bimap<unsigned int, std::string> RAW_VALUE_MAPS::RACE = {
	{1, "Human"},
	{2, "Elf"},
	{4, "Gnome"},
	{5, "Hobbit"},
	{8, "Dragonewt"}
};

bimap<unsigned int, std::string> RAW_VALUE_MAPS::GENDER = {
	{2, "Male"},
	{4, "Female"},
	{5, "???"}
};

bimap<unsigned int, std::string> RAW_VALUE_MAPS::ALIGNMENT = {
	{2, "Male"},
	{4, "Female"},
	{5, "???"}
};

bimap<unsigned int, std::string> RAW_VALUE_MAPS::CLASS = {
	{2, "Male"},
	{4, "Female"},
	{5, "???"}
};

bimap<unsigned int, std::string> RAW_VALUE_MAPS::TRAITS = {
	{0x00, "None"},
	{0x01, "Mage's Mage spell learning"},
	{0x02, "Bard's Mage spell learning"},
	{0x03, "Bishop's Mage spell learning"},
	{0x04, "Cleric's Priest spell learning"},
	{0x05, "Valkyrie's Priest spell learning"},
	{0x06, "Bishop's Priest spell learning"},
	{0x07, "Alchemist's spell learning"},
	{0x08, "Summoning spell learning"},
	{0x09, "Dispel"},
	{0x0A, "Lighting Physical Attack (aka can kill ghosts)"},
	{0x0B, "Thief skills"},
	{0x0C, "Lesser Thief Skills"},
	{0x0D, "Forging"},
	{0x0F, "Treatment"},
	{0x0E, "Transmutation"},
	{0x10, "Banking (Don't know what this does"},
	{0x11, "Herbology"},
	{0x12, "Identification"},
	{0x13, "Swift Attack"},
	{0x14, "Pursuit Attack"},
	{0x15, "Lower AC"},
	{0x16, "Brawler's multi-attack"},
	{0x17, "Tarot ability"},
	{0x18, "Instrument use"},
	{0x19, "Barrier ability"},
	{0x1A, "Special Resist UP"},
	{0x1B, "Dual Wielding"},
	{0x1C, "Instant Kill to all attacks"},
	{0x1D, "Vigilance"},
	{0x1E, "Physical Attack Up +1"},
	{0x1F, "Physical Attack Up"},
	{0x20, "Mage Spellpower Up +1"},
	{0x21, "Mage Spellpower Up"},
	{0x22, "Cleric Spellpower Up +1"},
	{0x23, "Cleric Spellpower Up"},
	{0x24, "Alchemist Spellpower Up +1"},
	{0x25, "Alchemist Spellpower Up"},
	{0x26, "Magical Power Up +1"},
	{0x27, "Magical Power Up"},
	{0x28, "Soul Release"},
	{0x29, "Equipment Removal"},
	{0x2A, "Steal"},
	{0x2B, "Disassemble"},
	{0x2C, "High Purity Synthesis (No % next to a level 4 character)"},
	{0x2D, "Apothecary Knowledge"},
	{0x2E, "Pursuit Sweep"},
	{0x2F, "Combat Instincts (No % next to a level 4 character)"},
	{0x30, "Alter Fate"},
	{0x31, "Ancient Rites"},
	{0x32, "Blood Oath"},
	{0x33, "Physical Attack Up +1 (Begins a series of repeats)"},
	{0x34, "Physical Attack UP"},
	{0x41, "High Purity Synthesis (100% next to a level 4 character)"},
	{0x43, "Pursuit Sweep"},
	{0x44, "Combat Instincts (50% next to a level 4 character)"},
	{0x48, "Holy Lance Art"},
	{0x49, "Court Sanctuary"},
	{0x4B, "Surprise Attack"}
};

bimap<unsigned int, std::string> RAW_VALUE_MAPS::SKILLS = {
	{0x65, "Brace"},
	{0x66, "Stronghold"},
	{0x67, "High Magic Source (negate barrier)"},
	{0x68, "Spirit Contract"},
	{0x69, "Magic Essence"},
	{0x6B, "Hand of Kindness"},
	{0x6D, "Pass-along Theft"},
	{0x6E, "Find Treasure"},
	{0x6F, "Tackle"},
	{0x75, "Restore Magic"},
	{0x76, "Divination"},
	{0x77, "Mysterious Bag"},
	{0x79, "War Rite"},
	{0x7C, "Chi Wave"},
	{0x7D, "Drunken Fist"},
	{0x80, "Song of Healing"},
	{0x86, "Master Therion (call all summons at once?)"},
	{0x88, "Sacrifice"},
	{0x89, "Absolute Barrier"},
	{0x8A, "Counterattack (???)"},
	{0x8C, "Swallow Killer"},
	{0x8D, "Replica"},
	{0x8F, "Cruelty"},
	{0x90, "Glutton for Punishment"}
};
