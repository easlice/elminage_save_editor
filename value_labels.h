#pragma once
#ifndef VALUE_LABELS_H
#define VALUE_LABELS_H

#include <string>
#include <map>
#include "bimap.h"

namespace RAW_VALUE_MAPS {
	extern bimap<unsigned int, std::string> RACE;
	extern bimap<unsigned int, std::string> GENDER;
	extern bimap<unsigned int, std::string> ALIGNMENT;
	extern bimap<unsigned int, std::string> CLASS;
	extern bimap<unsigned int, std::string> TRAITS;
	extern bimap<unsigned int, std::string> SKILLS;
}

#endif // VALUE_LABELS_H
