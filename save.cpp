#include "save.h"

void Save::read(std::ifstream& file) {
	Header h;
	h.read(file);
	this->header = h;
	while (file.good() && !file.eof() && doesFilePointAtCharacter(file)) {
		Character c;
		c.read(file);
		this->characters.push_back(c);
	}
	const unsigned int start_pos = file.tellg();
	file.seekg(0, std::ios::end);
	const unsigned int end_pos = file.tellg();
	this->rest_size = end_pos - start_pos;
	file.seekg(start_pos, std::ios::beg);
	this->rest = new char[this->rest_size];
	file.read(this->rest, this->rest_size);
}

void Save::write(std::ofstream& file) {
	this->header.write(file);
	for (Character c : this->characters) {
		c.write(file);
	}
	file.write(this->rest, this->rest_size);
}

void Save::print() {
    for (Character c : this->characters) {
        c.print();
    }
}
