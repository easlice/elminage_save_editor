#include "utils.h"

std::array<unsigned char, 4> get_uchar_array_from_uint32(const std::uint32_t in_val) {
	std::array<unsigned char, 4> arr = {""};
	std::uint32_t val = in_val;
	for (int i = 0; val > 0, i <= (8*4); ++i) {
		arr.data()[i/8] |= ((val & 0b1) << (i % 8));
		val >>= 1;
	}
	return arr;
}

std::uint32_t get_uint32_from_uchar_array(const std::array<unsigned char, 4> arr) {
	std::uint32_t num = 0;
	unsigned int size = arr.size();
	// If i is changed to unsign, the condition will need to be changed
	// because i will just rollover to another value above 0
	for (int i = size - 1; i >= 0; --i) {
		num += (1 << (i * 8)) * (unsigned int) arr[i];
	}
	return num;
}

std::array<unsigned char, 8> get_uchar_array_from_uint64(const std::uint64_t in_val) {
	std::array<unsigned char, 8> arr = {""};
	std::uint64_t val = in_val;
	for (int i = 0; val > 0, i <= (8*8); ++i) {
		arr.data()[i/8] |= ((val & 0b1) << (i % 8));
		val >>= 1;
	}
	return arr;
}

std::uint64_t get_uint64_from_uchar_array(const std::array<unsigned char, 8> arr) {
	std::uint64_t num = 0;
	unsigned int size = arr.size();
	// If i is changed to unsign, the condition will need to be changed
	// because i will just rollover to another value above 0
	for (int i = size - 1; i >= 0; --i) {
		num += (1 << (i * 8)) * (unsigned int) arr[i];
	}
	return num;
}

void print_array_as_hex (void *arr, unsigned int size) {
	unsigned char* tmp = (unsigned char*) arr;
	std::ios::fmtflags flags(std::cout.flags());
	for (unsigned int i = 0; i < size; ++i) {
		if (i > 0) {
			std::cout << " ";
		}
		PRINT_HEX(tmp[i]);
	}
	std::cout.flags(flags);
}

unsigned long long get_little_endian_long(unsigned char* arr, unsigned int size) {
	unsigned long long num = 0;
	for (int i = size - 1; i >= 0; --i) {
		num += (1 << (i * 8)) * (unsigned int) arr[i];
	}
	return num;
}

void print_traits(unsigned char* traits, unsigned int size) {
	std::ios::fmtflags flags(std::cout.flags());
	std::cout << "Traits:" << std::endl;
	for (int i = 0; i < size; ++i) {
		std::cout << "\t";
		PRINTLN(PRINT_HEX(traits[i]);PRINT_MAP_VALUE(traits[i],RAW_VALUE_MAPS::TRAITS));
	}
	std::cout.flags(flags);
}

bool doesFilePointAtCharacter(std::ifstream& file) {
	const unsigned int start_pos = file.tellg();
	char name[27];
	file.read(name, 27);
	file.seekg(start_pos, std::ios::beg);
	// Stupid check here, make sure the pattern is:
	// NOT 0's followed by only 0's in the name field
	if ((int) name[0] == 0) {
		return false;
	}
	bool only_zeros = false;
	for (int i = 0; i < 27; ++i) {
		if (only_zeros && (int) name[i] != 0) {
			return false;
		}
		if (!only_zeros && (int) name[i] == 0) {
			only_zeros = true;
			continue;
		}
	}
	return true;
}
