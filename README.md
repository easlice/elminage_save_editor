#Elminage Save Editor

This is a save game editor and viewer for the game Elminage Gothic, a party based dungeon crawler in the vein of the old Wizardry games, written in C++. More information about the game can be found [at this sales page](https://www.gog.com/game/elminage_gothic) and [at this wiki](http://elminage-gothic.wikia.com/).

**This is still very much a work in progress.**

------

## What works? ##
Good question. This project is really three pieces:

1. a base set of objects that represent a save game
2. a CLI for viewing and minimal character manipulation
3. a QT based GUI for viewing and editing characters

I'll try to summarize the status of each piece below.

#### Base Objects ####
Thing done:

* Base data structures for holding save game information.
* A number of byte fields found in a character file are already mapped and have mutators.
* The ability to read in and write out a save file.

To be done:

* map the rest of the races and skills.
* figure out how classes and alignment are mapped.
* make the character print method *far* less hacky
* many, many unknown bytes left to still figure out and map

#### CLI ####
Things done:

* The ability to see what characters are in a save file.
* The ability to see a detailed info dump of a character.
* The ability to check and see if any mapped values (skills, races, and classes, for example) of any character in the file are not already known.
* The ability to edit stats, age, and gp for any character in the save file.
* The ability to write the save state to a save file.

To be done:

* Allow editing more character fields.
* Allow the loading of a file from a program menu as well as part of the command line args
* Add in safe guards into the menu choices.

#### GUI ####
Thing done:

* Nothing yet

------

## How to Build##

There is a make file included. Just run 'make' to build the cli into a bin directory.

------

# Why do this? #

It started when I was playing the game but did not like the death by aging mechanic, so I wanted a way to change the characters ages. When I found the mod list on the wiki above, in which people had already started disassembling the save file, I decided that this would be a fun project to practice different techniques for mapping bit fields into a complex structure. As it turns out, I ended up enjoying this part of the project as much as I did playing the game!

I also figured it would be a good opportunity to learn QT, even though I haven't committed anything involving that yet.