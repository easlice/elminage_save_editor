#pragma once
#include <map>
#include <iostream>
#include <fstream>
#include <cstdint>
#include <vector>
#include <limits>
#include "save.h"
#include "character.h"
#include "header.h"
#include "utils.h"

int main(int argc, char* argv[]);
void list_char_names(std::vector<Character> &chars);
void write_save_to_file(Save game);
void get_char_info(std::vector<Character> &chars);
Character& select_character(std::vector<Character> &chars);
void verify_characters(std::vector<Character> &chars);
void edit_character(Character &c);
void edit_stats(Character &c);
void set_stat(std::string stat_name, const unsigned char current_val, Character& c, unsigned char (Character::*fpntr)(const unsigned char));
void edit_hp(Character &c);
void set_hp_current(Character &c);
void set_hp_max(Character &c);
void edit_age (Character &c);
void edit_gold (Character &c);
bool confirm();
void usage(char* name);
