#pragma once
#ifndef SAVE_H
#define SAVE_H

#include "header.h"
#include "character.h"
#include "utils.h"
#include <iostream>
#include <fstream>
#include <vector>

class Save {
	public:
		Header header;
		std::vector<Character> characters;

		void read(std::ifstream& file);
		void write(std::ofstream& file);
        void print();

		char* rest;
		unsigned int rest_size;

};

#endif // SAVE_H
